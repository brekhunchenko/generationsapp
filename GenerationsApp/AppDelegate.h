//
//  AppDelegate.h
//  GenerationsApp
//
//  Created by Yaroslav Brekhunchenko on 3/10/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

